/**
 * Car represents a car with passengers
 * @author Stefan Engvall
 * @version 1.0 2009-11-16
 */
public class Car extends Vehicle {

	/** 
	 * Car() is the constructor that calls the superclass 
	 * construction with the number of passengers, space 
	 * requirements and maximum nubmber of passengers.
	 * @param passengers - The number of passengers
	 */
	public Car(int passengers) {
		super(passengers, 5, 4);		
	}

}
