/**
 * FerryMain är en klass som testar metoderna i klassen BigFerry
 * @author Stefan engvall
 * @version 1.0 2009-11-16
 */
public class FerryMain {
	
	private static boolean verbose = true; 	// Visa felmeddelanden eller inte
	
	public static void main(String[] args) {
		
		// Skapa några olika fordon och passagerare
		Bicycle cykel = new Bicycle();	// +1 passagerare		
		Car bil1 = new Car(5);  		// har för många passagerare
		Car bil2 = new Car(2);		
		Passenger passagerare1 = new Passenger();
		Passenger passagerare2 = new Passenger();
		
		// Skapa en färja 
		if(verbose) System.out.println("\n\n" + "Skapar färja");
		BigFerry myFerry = new BigFerry(10,5);
			
		if(verbose) System.out.println("Maximalt utrymme: " + myFerry.getMax_space());
		if(verbose) System.out.println("Maximalt antal passagerare: " + myFerry.getMax_pass());
		
		// Lägg till fordon och passagerare till färjan
		if (verbose) System.out.println("\nCykel embarkerar ");
		myFerry.embark(cykel);
		if (verbose) System.out.print("\nBil1 embarkerar med 5 pass. - ska ge fel: ");
		myFerry.embark(bil1);
		if (verbose) System.out.print("\nCykel emarkerar igen - ska ge fel: ");
		myFerry.embark(cykel);  							
		if (verbose) System.out.println("\nBil2 embarkerar med 2 pass.");
		myFerry.embark(bil2);		
		if (verbose) System.out.println("\nPassagerare1 går ombord.");
		myFerry.embark(passagerare1);
		if (verbose) System.out.print("\nPassagerare1 går ombord igen - ska ge fel: ");
		myFerry.embark(passagerare1);						
		if (verbose) System.out.println("\nPassagerare2 går ombord.");
		myFerry.embark(passagerare2);
		
		
		// Skriv ut färjans last och ekonomi
		if(verbose) myFerry.showStatus();
		
		// Töm färjan
		if (verbose) System.out.println("\nTöm färjan");
		myFerry.disembark();
		
		// Skriv ut färjans last och ekonomi
		if(verbose) myFerry.showStatus();
		
		// Embarkera bil2 (med två pass) och passagerare1
		if (verbose) System.out.println("\nBil2 embarkeras med 2 pass.");
		myFerry.embark(bil2);		
		if (verbose) System.out.println("\nPassagerare1 går ombord.");
		myFerry.embark(passagerare1);
		
		// skriv ut ekonomi, fordon och passagerare
		if(verbose) myFerry.showStatus();
		
		// tjuven kommer
		if(verbose) System.out.println("\n\n" + "Tjuven tar kassan!!");
		myFerry.emptyCash();
		
		// skriv ut ekonomi, fordon och passagerare
		if(verbose) myFerry.showStatus();
	}	

}
