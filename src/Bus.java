/**
 * Bus represents a bus with passengers
 * @author Stefan Engvall
 * @version 1.0 2009-11-16
 */
public class Bus extends Vehicle {

	/** 
	 * Bus() is the constructor that calls the superclass 
	 * construction with the number of passengers, space 
	 * requirements and maximum nubmber of passengers.
	 * @param passengers - The number of passengers
	 */
	public Bus(int passengers) {
		super(passengers, 20, 20);
	}

}
