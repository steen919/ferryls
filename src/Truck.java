/**
 * Truck represents a truck with passengers
 * @author Stefan Engvall
 * @version 1.0 2009-11-16
 */
public class Truck extends Vehicle {

	/** 
	 * Truck() is the constructor that calls the superclass 
	 * construction with the number of passengers, space 
	 * requirements and maximum nubmber of passengers.
	 * @param passengers - The number of passengers
	 */
	public Truck(int passengers) {
		super(passengers, 40, 2);
	}

}
