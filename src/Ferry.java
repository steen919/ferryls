import java.util.Iterator;

/**
 * Ferry is an interface for common methods used on a ferry
 * @author Stefan Engvall
 * @version 1.0 2009-11-16
 */
public interface Ferry  {
	
	/** Number of passengers on board */
	int countPassengers(); 
	
	/** Used vehicle space. One car is 1 */
	int countVehicleSpace();                 
	
	/** Earned money **/
	int countMoney();                        
	   
	/** Vehicle iterator */
	@SuppressWarnings("unchecked")
	Iterator iterator();                     
	   
	/** Embark vehicle, warning if not enough space */
	void embark(Vehicle v);                  
	
	/** Embark passenger, warning if not enough room */
	void embark(Passenger p);                
	
	/** Clear (empty) ferry. The money earned remains, i.e., is not reset to zero */
	void disembark();                        
	
	/** true if we can embark vehicle v */
	boolean hasSpaceFor(Vehicle v);          
	
	/** true if we can embark another passenger */
	boolean hasRoomForPassenger(); 			
	
	/** Nice looking ferry status print out */
	String toString();                       
}