/**
 * Bicycle represents a bicycle with one passenger (the rider)
 * @author Stefan Engvall
 * @version 1.0 2009-11-16
 */
public class Bicycle extends Vehicle {
	
	/**
	 * Bicycle() is the constructor that calls the superclass 
	 * constructor with the number of passengers, space 
	 * requirements and maximum number of passengers.
	 */
	public Bicycle() {
		super(1, 1, 1);		
	}

}