/**
 * Vehicle represents common vehicles
 * @author Stefan Engvall
 * @version 1.0 2009-11-16
 */
public class Vehicle {
	
	private int passengers;
	private int spacereq;
	private int max_passengers;
	
	/**
	 * Vehicle() is the constructor that takes the arguments 
	 * for number of passengers, space requirement and maximum 
	 * number of passengers.
	 * @param passengers - Number of passengers
	 * @param spacereq - Required space for the vehicle
	 * @param max_passengers - Maximum number of passengers the vehicle can take
	 */
	public Vehicle(int passengers, int spacereq, int max_passengers) {
		this.passengers = passengers;
		this.spacereq = spacereq;
		this.max_passengers = max_passengers;
	}
	
	/**
	 * getPassengers() returns the number of passengers in the vehicle
	 * @return Number of passengers
	 */
	protected int getPassengers() {
		return passengers;
	}

	/**
	 * getSpaceReq() returns the required space for the vehicle.
	 * @return Required space
	 */
	protected int getSpacereq() {
		return spacereq;
	}
	
	/**
	 * getMax_passengers() returns the maximum number of passengers 
	 * that the vehicle can take.
	 * @return The maximum number of passengers
	 */
	protected int getMax_passengers() {
		return max_passengers;
	}

}
