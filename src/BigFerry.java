import java.util.ArrayList;
import java.util.Iterator;

/**
 * BigFerry represents a ferry that can take vehicles and 
 * passengers. BigFerry implements the Ferry interface.
 * @author Stefan Engvall
 * @version 2.0 2009-11-16
 */
public class BigFerry implements Ferry{
	
	private int max_space;			// Maximum space for vehicles
	private int max_pass;			// Maximum number of passengers
	private ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();				// ArrayList to hold the vehicles
	private ArrayList<Passenger> passengers = new ArrayList<Passenger>();		// ArrayList to hold the passengers
	private int cash = 0;			// Variable to keep hold of the paid fees.
	
	private boolean debug = false;	// Turn the debug messages on or off
	
	/**
	 * BigFerry is the contructor that takes the 
	 * maximum space and passengers as parameters.
	 * @param space - The ferry's maximum vehicle space
	 * @param pass - The ferry's maximum number of passengers
	 */
	public BigFerry(int space, int pass) {
		this.max_space = space;
		this.max_pass = pass;
	}
	
	/**
	 * getMax_space() returns the ferry's maximum 
	 * space for vehicles
	 * @return Maximum space for vehicles
	 */
	public int getMax_space() {
		return max_space;
	}
	
	/**
	 * getMax_passengers() returns the ferry's maximum 
	 * number of passengers
	 * @return Maximum number of passengers
	 */
	public int getMax_pass() {
		return max_pass;
	}

	@Override
	public int countMoney() {		
		return cash;
	}

	@Override
	public int countPassengers() {
		return passengers.size();
	}

	@Override
	public int countVehicleSpace() {
		int space = 0;
		
		// Go through all vehicles using an iterator and add their space to variable space
		for ( Iterator<Vehicle> it = iterator() ; it.hasNext() ;  ) {			
			Vehicle v = it.next();			
			space += v.getSpacereq();
		}
		
		double carSpace = space / 5.0;
		
		// Return the space in car sizes
		space = (int)Math.ceil(carSpace);
		
		return space;
	}
	
	public int getFreeSpace() {
		return max_space - countVehicleSpace();
	}

	@Override
	public void disembark() {
		passengers.clear();
		vehicles.clear();		
	}

	@Override
	public void embark(Vehicle v) {
		
		// Check if the number of passengers in the car is less the the maximum else throw error message
		if ( v.getPassengers() <= v.getMax_passengers() ) {
			
			
			// Check if the vehicle is alredy onboard
			boolean onboard = false;			
			for ( int i = 0 ; i < passengers.size() ; i++ ) {
				if ( vehicles.get(i).equals(v)) onboard = true;
			}		
			
			
			// If vehicle already onboard throw error message else check for available space
			if (onboard) {
				System.out.println("Fordonet är redan ombord!");
			} else {
				
				// If there is sufficient space... (else throw error message)
				if ( hasSpaceFor(v) ) {
					
					// ... check if there is enough space for the passengers 
					boolean hasRoomFor = true;
					for ( int i = 0 ; i < v.getPassengers() ; i++ ) {
						if (!hasRoomForPassenger()) hasRoomFor = false;
					}		
					
					// If there is sufficient space for the passengers, calculate the cost for
					// the vehicle and its passengers and add the vehicles and the passengers, otherwise
					// throw error message
					if ( hasRoomFor ) {
						cash += cost(v);
						vehicles.add(v);
						for ( int i = 0 ; i < v.getPassengers() ; i++ ) {
							passengers.add(new Passenger());
						}
					} else System.out.println("Passagerarna får ej plats! Fordonet får inte embarkera!");
						
				} else System.out.println("Fordon får ej plats!");
			}
		} else System.out.println("För många passagerare! Fordonet får inte embarkera!");
		
	}
	
	/**
	 * cost() calculates the cost of each vehicle including its passangers
	 * @param v - The vehicle to calculate cost for
	 * @return The cost for the vehicle and the passengers
	 */
	private int cost(Vehicle v) {
		int cost = 0;
		
		// Depending on type of vehicle add the cost for the vehicle and 
		// the passengers to the variable cost
		if ( v instanceof Bicycle ) cost += 40;
		else if ( v instanceof Car ) {
			cost += 100;
			for ( int i = 0 ; i < v.getPassengers() ; i++ ) {
				cost += 15;
			}
		} else if ( v instanceof Bus ) {
			cost += 200;
			for ( int i = 0 ; i < v.getPassengers() ; i++ ) {
				cost += 10;
			}
		} else if ( v instanceof Truck ) {
			cost += 300;
			for ( int i = 0 ; i < v.getPassengers() ; i++ ) {
				cost += 15;
			}
		}	
		
		return cost;
	}

	@Override
	public void embark(Passenger p) {
		
		// Check if passenger already onboard
		boolean onboard = false;
		for ( int i = 0 ; i < passengers.size() ; i++ ) {
			if ( passengers.get(i).equals(p)) onboard = true;
		}	
		
		// If already onboard throw error message else ...
		if (onboard) {
			System.out.println("Passageraren är redan ombord!");
		} 
		
		// Check if there is enough space else ...
		else if (hasRoomForPassenger() ) {
			if ( debug ) System.out.println("Passageraren embarkerad!");
			passengers.add(p);
			cash += 20;
		} 
		// throw error messsage
		else {
			System.out.println("Passageraren får tyvärr inte plats!");
		}
	}

	@Override
	public boolean hasRoomForPassenger() {
		return ( (this.max_pass - countPassengers() - 1) >=0 );
	}

	@Override
	public boolean hasSpaceFor(Vehicle v) {
		if ( (this.max_space - countVehicleSpace() - v.getSpacereq() ) >=0 ) return true;
		else return false;
	}

	@Override
	public Iterator<Vehicle> iterator() {		
		return vehicles.iterator();
	}
	
	@Override
	public String toString() {
		String str = "";
		
		// Variables to keep number of each type of vehicle
		int Bicycles = 0;
		int Cars = 0;
		int Buses = 0;
		int Trucks = 0;
		
		// Go through all vehicles using iterator
		for ( Iterator<Vehicle> it = iterator() ; it.hasNext() ;  ) {
			
			Vehicle v = it.next();
			
			// Increment the variable for present type of vehicle
			if (v instanceof Bicycle) Bicycles++;
			else if (v instanceof Car) Cars++;
			else if (v instanceof Bus) Buses++;
			else if (v instanceof Truck) Trucks++;
			
		}
		
		// Add number of each vehicle type, number of passsengers and 
		// the amount of cash to the ouput string
		str += "------------ Fordon och passagerare -------------\n";
		str += " Cyklar: " + Bicycles + "\n";
		str += " Bilar: " + Cars + "\n";
		str += " Bussar: " + Buses + "\n";
		str += " Lastbilar: " + Trucks + "\n";
		str += " Passagerare: " + passengers.size() + "\n\n";
		str += "------------------- Ekonomi -----------------------\n";
		str += " Kassa:" + this.countMoney() + "\n";
		str += "---------------------------------------------------";
		
		return str;
	}
	
	/**
	 * emptyCash() empties the cashregister
	 */
	public void emptyCash() {
		cash = 0;
	}
	
	/**
	 * showStatus() prints out the current space used and available, 
	 * number of vehicles and passengers, and finally the amount of cash.
	 */
	public void showStatus() {
		// Skriv ut färjans last och ekonomi
		System.out.println("\n\n" + "Status:");
		System.out.println("Använt utrymme (i antal bilar): " + this.countVehicleSpace());
		System.out.println("Ledigt utrymme (i antal bilar): " + this.getFreeSpace());
		System.out.println(this.toString());
	}

}
